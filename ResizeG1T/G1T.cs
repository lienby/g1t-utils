﻿using System;
using System.IO;
using ManagedSquish;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace AtelierFiles
{
    static class G1T
    {

        private static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
        public static Bitmap DownsizeBy(Bitmap image, int Percentage)
        {
            return ResizeImage(image, image.Width / Percentage, image.Height / Percentage);
        }

        public static void WriteUInt32(this Stream s, uint num)
        {
            s.Write(BitConverter.GetBytes(num), 0, 4);
        }

        public static uint ReadUInt32(this Stream s)
        {
            int b1 = s.ReadByte();
            int b2 = s.ReadByte();
            int b3 = s.ReadByte();
            int b4 = s.ReadByte();

            return (uint)(b4 << 24 | b3 << 16 | b2 << 8 | b1);
        }

        public static int GetNumberOfTextures(String G1TPath)
        {
            Stream fs = new FileStream(G1TPath, FileMode.Open);
            fs.Seek(0x10, SeekOrigin.Current);
            UInt32 NumberOfTextures = ReadUInt32(fs);
            fs.Close();
            return (int)NumberOfTextures;
        }

        public static void G1T2PNG(String G1TPath, String PNGPath, int ImageIndex)
        {
            DecompressG1T(G1TPath, ImageIndex).Save(PNGPath,ImageFormat.Png);
        }

        public static Byte CreateDimensionsByte(int Width, int Height)
        {
            return (Byte)((int)Math.Log(Height, 2) << 4 | (int)Math.Log(Width, 2));
        }


        private static byte[] CompressImage(byte[] rgba, int width, int height, SquishFlags flags)
        {

            MemoryStream ms = new MemoryStream();

            // initialise the block output
            int targetBlock = 0;
            int bytesPerBlock = (flags.HasFlag(SquishFlags.Dxt1) ? 8 : 16);

            // loop over blocks
            for (int y = 0; y < height; y += 4)
            {
                for (int x = 0; x < width; x += 4)
                {
                    // build the 4x4 block of pixels
                    byte[] sourceRgba = new byte[16 * 4];
                    byte targetPixel = 0;
                    int mask = 0;

                    for (int py = 0; py < 4; ++py)
                    {
                        for (int px = 0; px < 4; ++px)
                        {
                            // get the source pixel in the image
                            int sx = x + px;
                            int sy = y + py;

                            // enable if we're in the image
                            if (sx < width && sy < height)
                            {
                                // copy the rgba value
                                for (int i = 0; i < 4; ++i)
                                {
                                    sourceRgba[targetPixel] = rgba[i + 4 * (width * sy + sx)];
                                    targetPixel++;
                                }

                                // enable this pixel
                                mask |= (1 << (4 * py + px));
                            }
                            else
                            {
                                // skip this pixel as its outside the image
                                targetPixel += 4;
                            }
                        }
                    }



                    var length = height * width;
                    var bytes = new byte[bytesPerBlock];

                    // compress it into the output
                    IntPtr RGBA = Marshal.UnsafeAddrOfPinnedArrayElement(sourceRgba, 0);
                    IntPtr Blocks = Marshal.UnsafeAddrOfPinnedArrayElement(bytes, 0);

                    Squish.Compress(RGBA, Blocks, flags);

                    ms.Write(bytes, 0x00, bytes.Length);

                    // advance
                    targetBlock += bytesPerBlock;
                }
            }
            ms.Seek(0x00, SeekOrigin.Begin);
            return ms.ToArray();
        }

        public static void VitafyG1T(String G1TPath)
        {
            int NumberOfTextures = GetNumberOfTextures(G1TPath);
            for(int i = 1; i <= NumberOfTextures; i++)
            {
                Bitmap bmp = G1T.DecompressG1T(G1TPath,i);
                bmp = G1T.DownsizeBy(bmp, 2);
                G1T.CompressG1T(G1TPath,i , bmp,true);
            }

        }

        public static void CompressG1T(String G1TPath, int ImageIndex, Bitmap NewBitmap, bool changeFormat = false)
        {
            Stream fs = new FileStream(G1TPath, FileMode.Open,FileAccess.ReadWrite);
            fs.Seek(0x8, SeekOrigin.Current);
            UInt32 FileSize = ReadUInt32(fs);
            UInt32 headerSize = ReadUInt32(fs);
            UInt32 NumberOfTextures = ReadUInt32(fs);
            fs.Seek(0x4, SeekOrigin.Current);

            fs.Position = headerSize;
            uint bytesUnknownData = ReadUInt32(fs);
            fs.Position = headerSize + bytesUnknownData;
            for (int i = 1; i <= NumberOfTextures; i++)
            {
                int Height = NewBitmap.Height;
                int Width = NewBitmap.Width;

                Byte Mipmaps = (Byte)fs.ReadByte();
                Byte Type = (Byte)fs.ReadByte();
                Byte OldDimensions = (Byte)fs.ReadByte();

                int OHeight = (1 << (OldDimensions >> 4));
                int OWidth = (1 << (OldDimensions & 0x0F));

                SquishFlags format;
                uint BitPerPixel;

                switch (Type)
                {
                    case 0x06: format = SquishFlags.Dxt1; BitPerPixel = 4; break;
                    case 0x10: format = SquishFlags.Dxt1; BitPerPixel = 4; break;
                    case 0x59: format = SquishFlags.Dxt1; BitPerPixel = 4; break;
                    case 0x5B: format = SquishFlags.Dxt5; BitPerPixel = 8; break;
                    default: format = SquishFlags.Dxt5; BitPerPixel = 8; break;
                }

                long textureSize = (OWidth * OHeight * BitPerPixel) / 8;
                long compressedSize = (Width * Height * BitPerPixel) / 8;

                if (i != ImageIndex)
                {
                    fs.Seek(textureSize + 0x11, SeekOrigin.Current);
                }
                else
                {
                    fs.Seek(-1, SeekOrigin.Current);
                    if(changeFormat)
                    {
                        fs.Seek(-1, SeekOrigin.Current);
                        fs.WriteByte(0x59);
                        format = SquishFlags.Dxt1;
                        BitPerPixel = 4;
                        compressedSize = (Width * Height * BitPerPixel) / 8;
                        Type = 0x59;
                    }
                    Byte Dimensions = CreateDimensionsByte(Width, Height);
                    fs.WriteByte(Dimensions);
                    Console.WriteLine("Writing new Dimensions Byte...");

                    Console.WriteLine("Image: " + i.ToString());
                    Console.WriteLine("Mipmaps: " + Mipmaps.ToString("X2"));
                    Console.WriteLine("Type: " + Type.ToString("X2") + " (" + format.ToString() + ")");
                    Console.WriteLine("Dimensions: " + Dimensions.ToString("X2") + " (" + Width.ToString() + "x" + Height.ToString() + ")");
                    Console.WriteLine("Texture Size: " + compressedSize.ToString("X") + "B");


                    

                    fs.Seek(0x11, SeekOrigin.Current);

                    Byte[] ImageBytes = BitmapToRGBA(NewBitmap);

                    Console.Write("Compressing...");
                    Byte[] CompressedImage = CompressImage(ImageBytes, Width, Height,format);
                    Console.WriteLine("DONE!");


                    if(CompressedImage.Length < textureSize)
                    {
                        
                        long OgPos = fs.Position;
                        fs.Seek(textureSize, SeekOrigin.Current);
                        Byte[] RestOfFile = new Byte[fs.Length - fs.Position];
                        fs.Read(RestOfFile, 0x00, RestOfFile.Length);
                        fs.Seek(OgPos, SeekOrigin.Begin);
                        fs.SetLength(OgPos);
                        fs.Write(CompressedImage, 0x00, CompressedImage.Length);
                        fs.Write(RestOfFile, 0x00, RestOfFile.Length);
                        fs.Seek(0x8, SeekOrigin.Current);
                        WriteUInt32(fs,(UInt32)fs.Length);
                    }
                    else
                    {
                        fs.Write(CompressedImage, 0x00, CompressedImage.Length);
                    }
                    

                    fs.Close();

                    return;

                }

            }

        }

        public static Bitmap DecompressG1T(String G1TPath, int ImageIndex)
        {
            Stream fs = new FileStream(G1TPath, FileMode.Open,FileAccess.Read);
            fs.Seek(0xC, SeekOrigin.Current);
            UInt32 headerSize = ReadUInt32(fs);
            UInt32 NumberOfTextures = ReadUInt32(fs);
            fs.Seek(0x4, SeekOrigin.Current);
            
            fs.Position = headerSize;
            uint bytesUnknownData = ReadUInt32(fs);
            fs.Position = headerSize + bytesUnknownData;

            for (int i = 1; i <= NumberOfTextures; i++)
            {

                Byte Mipmaps = (Byte)fs.ReadByte();
                Byte Type = (Byte)fs.ReadByte();
                Byte Dimensions = (Byte)fs.ReadByte();

                int Height = (1 << (Dimensions >> 4));
                int Width = (1 << (Dimensions & 0x0F));

                SquishFlags format;
                uint BitPerPixel;

                switch (Type)
                {
                    case 0x06: format = SquishFlags.Dxt1; BitPerPixel = 4; break;
                    case 0x10: format = SquishFlags.Dxt1; BitPerPixel = 4; break;
                    case 0x59: format = SquishFlags.Dxt1; BitPerPixel = 4; break;
                    case 0x5B: format = SquishFlags.Dxt5; BitPerPixel = 8; break;
                    default: format = SquishFlags.Dxt5; BitPerPixel = 8; break;
                }


                long textureSize = (Width * Height * BitPerPixel) / 8;

                if(i != ImageIndex)
                {
                    fs.Seek(textureSize+0x11, SeekOrigin.Current);
                }
                else
                {

                    Console.WriteLine("Image: " + i.ToString());
                    Console.WriteLine("Mipmaps: " + Mipmaps.ToString("X2"));
                    Console.WriteLine("Type: " + Type.ToString("X2") + " (" + format.ToString() + ")");
                    Console.WriteLine("Dimensions: " + Dimensions.ToString("X2") + " (" + Width.ToString() + "x" + Height.ToString() + ")");
                    Console.WriteLine("Texture Size: " + textureSize.ToString("X") + "B");


                    Console.Write("Decompressing...");

                    fs.Seek(0x11, SeekOrigin.Current);
                    Byte[] TexData = new Byte[textureSize];
                    fs.Read(TexData, 0, TexData.Length);
                    fs.Close();

                    Byte[] Decompressed = Squish.DecompressImage(TexData, Width, Height, format);

                    Console.WriteLine("DONE!");

                    return ConvertToBitmap(Decompressed,Width,Height);

                }
                
            }

            throw new Exception("ImageIndexIsInvalid");

        }

        private static Byte[] BitmapToRGBA(Bitmap bmp)
        {
            Console.Write("Creating RGBA Array...");

            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadOnly, bmp.PixelFormat);


            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];
            IntPtr ptr = bmpData.Scan0;

            Marshal.Copy(ptr, rgbValues, 0, bytes);


            //Convert ARGB to RGBA
            byte[] RGBABytes = new byte[rgbValues.Length];

            for (int x = 0; x < rgbValues.Length; x += 4)
            {
                byte[] pixel = new byte[4];
                Array.Copy(rgbValues, x, pixel, 0, 4);

                byte b = pixel[0];
                byte g = pixel[1];
                byte r = pixel[2];
                byte a = pixel[3];

                byte[] newPixel = new byte[] {  r, g, b, a };

                Array.Copy(newPixel, 0, RGBABytes, x, 4);
            }

            bmp.UnlockBits(bmpData);
            Console.WriteLine("DONE!");
            return RGBABytes;
        }

        private static Bitmap ConvertToBitmap(Byte[] RGBABytes, int Width, int Height)
        {
            Console.Write("Creating Bitmap...");


            Bitmap bmp = new Bitmap(Width, Height, PixelFormat.Format32bppArgb);

            //Convert RGBA to ARGB
            byte[] ARGBBytes = new byte[RGBABytes.Length];

            for (int x = 0; x < RGBABytes.Length; x += 4)
            {
                byte[] pixel = new byte[4];
                Array.Copy(RGBABytes, x, pixel, 0, 4);

                byte r = pixel[0];
                byte g = pixel[1];
                byte b = pixel[2];
                byte a = pixel[3];

                byte[] newPixel = new byte[] { b, g, r, a };

                Array.Copy(newPixel, 0, ARGBBytes, x, 4);
            }

            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, bmp.PixelFormat);

            IntPtr ptr = bmpData.Scan0;

            Marshal.Copy(ARGBBytes, 0, ptr, ARGBBytes.Length);

            bmp.UnlockBits(bmpData);

            Console.WriteLine("DONE!");
            return bmp;
        }
    }
}
