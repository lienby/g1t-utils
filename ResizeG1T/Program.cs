﻿using AtelierFiles;
using System;
using System.Drawing;
using System.IO;

namespace ResizeG1T
{
    class Program
    {
       static void Main()
        {
            string[] files = Directory.GetFiles(@"C:\Users\SilicaAndPina\Desktop\PKGs\res_en\ui", "*.g1t", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                Console.WriteLine("Vitafying: " + file);
                G1T.VitafyG1T(file);
            }


            Console.WriteLine("Done!!");
            Console.ReadKey();

        }
    }
}
